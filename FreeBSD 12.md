# FreeBSD 12 instalacija

## Instalacija

Skini ISO ili bilo koji drugi tip slike i spremi ga na USB disk ili CD/DVD.

Prilikom instalacije paziti na sljedeće detalje:

 - Zauzeće diska: cijeli disk
 - Particijska tablica: GPT
 - Swap particija: Ne
 - Datotečni sustav: UFS2, uključiti TRIM

Podesiti repozitorij paketa na najnoviji (zamijeniti "quarterly" sa "latest") u datoteci `/etc/pkg/FreeBSD.conf`.

Pokrenuti naredbu `pkg` da se instalira upravitelj paketima. Nakon toga pokrenuti `pkg update` da se ažurira popis raspoloživih programa.

Instalirati nužne pakete:
```bash
pkg install nano xorg fish doas webfonts drm-kmod sddm cups xsane sane-backends \
            avahi avahi-libdns nss_mdns
```

Dodati korisnika u sljedeće grupe:
```bash
pw group mod video -m USERNAME
pw group mod wheel -m USERNAME
pw group mod operator -m USERNAME
```

Promijeniti ljusku korisnika na fish:
```bash
chsh -s fish USERNAME
```

Ovlastiti korisnika kao superuser-a na način da se uredi datoteka `/usr/local/etc/doas.conf` i doda sljedeći red:
```
permit USERNAME as root
```

Učitati upravljački program za AMD-ovu grafičku karticu na način da se u datoteku `/etc/rc.conf` doda sljedeći red:
```
kld_list="amdgpu"
```

Dodati sljedeću liniju u `/etc/fstab`:
```
proc    /proc   procfs  rw  0   0
```

Dodati sljedeću liniju u `/etc/rc.conf`:
```
avahi_daemon_enable="YES"
cupsd_enable="YES"
```

Instalirati ostale programe:
```bash
pkg install firefox chromium vlc go kicad cockroach node yarn git \
            libreoffice blender redshift python3 hplip gimp vscode \
            p7zip screenfetch gitg unrar kicad-library-footprints \
            kicad-library-packages3d kicad-library-symbols \
            kicad-library-tmpl qbittorrent lyx texlive-full xmedcon
            imagemagick7 kdeconnect-kde k3b kio-gdrive npm
```

## Postavljanje hrvatskog jezika prilikom prijave korisnika

U datoteku `/etc/login.conf` na kraj zapisa default dodati sljedeće redove:
```
:charset=UTF-8:\
:lang=hr_HR.UTF-8:
```

Prvi red je obvezan, no drugi treba postaviti samo ako se žele hrvatske postavke sustava.

**Napomena:** Ne zaboraviti staviti znak \ na kraj zadnjeg reda prije dodavanja navedenih redova.

Pokrenuti naredbu `cap_mkdb /etc/login.conf`.

## Ostale postavke

Bitne postavke koje je potrebno upisati u `/etc/rc.conf` (ako to već nije napravljeno):
```
clear_tmp_enable="YES"
hostname="GlavniPC"
keymap="hr.kbd"
ifconfig_igb0="DHCP"
ntpd_enable="YES"
powerd_enable="YES"
kld_list="aesni"
zfs_enable="YES"
devfs_system_ruleset="system"
```

Postavke koje je potrebno upisati u `/etc/devfs.rules`:
```
[system=10]
add path 'cd*' mode 0666
add path 'pass*' mode 0666
add path 'xpt0' mode 0666
add path 'video*' mode 0666
add path 'uhid*' mode 0666
```

Postavke koje je potrebno upisati u `/etc/hosts`:
```
::1                     localhost localhost.my.domain GlavniPC
127.0.0.1               localhost localhost.my.domain GlavniPC
```

## Rješavanje problema

Ako se sustav pokreće u EFI modu i koristi se AMD-ova grafička kartica dodati sljedeći red u datoteku `/boot/loader.conf`:
```
hw.syscons.disable=1
```

Ako zvučna kartica radi probleme, dodati sljedeću liniju u `/etc/sysctl.conf`:
```
dev.hdac.%d.polling=1
```
gdje je `%d` redni broj zvučne kartice koja uzrokuje probleme (redne brojeve saznati sa `sysctl dev.hdac`).

Ako rad u grafičkom okruženju šteka, potrebno je promijeniti timer sa `LAPIC` na `HPET` (ili obratno) dodavanjem sljedećeg zapisa u datoteku `/etc/sysctl.conf`:
```
kern.eventtimer.timer=HPET
```

Odabrati onog koji ima najveću kvalitetu (najviši broj): `sysctl kern.eventtimer | grep quality`.


Ako zbog promjene timera sustav počne sporije reagirati, treba vratiti timer na prethodni i isključiti tickeless način rada na način da se uključi periodični timera u datoteci `/etc/sysctl.conf`:
```
kern.eventtimer.periodic=1
```

## Optimizacije za upotrebu na desktop računalima

U datoteku `/boot/loader.conf` upisati sljedeće:
```
kern.maxproc="100000"
kern.ipc.shmseg="1024"
kern.ipc.shmmni="1024"
net.link.ifqmaxlen="2048"
net.inet.tcp.soreceive_stream="1"
```

## Instalacija bootloader-a

Ako se koristi neki drugi sustav, npr. Linux, potrebno je omogućiti da se FreeBSD može odabrati u izborniku boot manager-a.

U datoteci `/etc/grub.d/40-custom` upisati sljedeće:
```
exec tail -n +3 $0

menuentry "FreeBSD" {
insmod ufs2
set root=(hd3,msdos1)
kfreebsd=/boot/loader
}

```

Pokrenuti naredbu `update-grub2`.

## Instalacija MATE desktopa

Instalirati MATE sa `pkg install mate`.

Dodati sljedeće linije u `/etc/rc.conf`:
```
hald_enable="YES"
dbus_enable="YES"
sddm_enable="YES"
```

## Instalacija GNOME3 desktopa

Instalirati GNOME3 sa `pkg install gnome3`.

Dodati sljedeće linije u `/etc/rc.conf`:
```
hald_enable="YES"
dbus_enable="YES"
gdm_enable="YES"
gnome_enable="YES"
sddm_enable="YES"
```

## Instalacija KDE desktopa

Instalirati KDE sa `pkg install kde5`.

U slučaju da se želi minimalna instalacija, potrebno je sa instalirati sljedeće pakete:
```bash
pkg install kde-baseapps plasma5-plasma kdeutils kdegraphics kmix
```

Dodati sljedeće linije u `/etc/rc.conf`:
```
hald_enable="YES"
dbus_enable="YES"
sddm_enable="YES"
```

## Korisničke postavke

Pokrenuti podešavanje fish ljuske naredbom `fish_config` i odabrati lava temu.

Ako se ne koristi disply manager (sddm ili sličan) potrebno je u datoteku `.xinitrc` upisati sljedeće:
```
exec mate-session
ili
exec xfce4-session
ili
exec cinnamon-session
itd.
```

### Postavke fontova

Da bi se poboljšao izgled fontova potrebno je isključiti bitmapirane fontove na sljedeći način:
```bash
cd /usr/local/etc/fonts/conf.d
ln -s ../conf.avail/70-no-bitmaps.conf
fc-cache -f
```

### MATE postavke

U datoteci `/usr/local/etc/PolicyKit/PolicyKit.conf` dodati kao prvi unos unutar korjenskog zapisa:
```xml
<match action="org.freedesktop.hal.storage.mount-removable">
<return result="yes"/>
</match>
<match action="org.freedesktop.hal.storage.mount-fixed">
<return result="yes"/>
</match>
```

Pokrenuti sljedeću naredbu:
```bash
gconftool-2 --set "/system/storage/default_options/vfat/mount_options" \
            --type list --list-type=string "[longnames,-u=,-L=en_US.UTF-8]"
```

Ostale postavke:
 - Podesiti subpixel hinting na full
 - Postaviti hrvatsku tipkovnicu
 - Postaviti fontove na 12
 - Postaviti da se pokreće Redshift prilikom pokretanja desktopa (`redshift -l geoclue2 -t 6500:4500`)

### GNOME3 postavke

Općenite postavke:

 - Hrvatska tipkovnica
 - Nightshift

### KDE postavke

Općenite postavke:

 - Hrvatska tipkovnica
 - Nightshift

## Instalacija printera

Otvoriti stranicu `http://localhost:631` i dodati printer HP Smart Tank 510 sa PPD datotekom `/usr/local/share/ppd/HP/hp-smart_tank_510_series.ppd.gz`.
Instalirati printer kroz HPLIP naredbom `hp-setup -i 192.168.5.38`.

## Instalacija web kamere

Pomoću naredbe `usbconfig` saznati ime uređaja. Ime uređaja se kasnije upisuje u datoteku `/etc/rc.conf`.

Dodati sljedeće linije u `/etc/rc.conf`:
```
webcamd_enable="YES"
webcamd_device_0_name="Vimicro Corp. Venus USB2.0 Camera"
```

ili

```
webcamd_enable="YES"
webcamd_0_flags="-d ugen0.2"
```

Dodati sljedeće linije u `/boot/loader.conf`:
```
cuse_load="YES"
```

Izvršiti naredbu `pw groupmod webcamd -m USERNAME`

## Podešavanje multimedijske tipkovnice

Instalirati potrebne pakete:
```bash
pkg install xdotool
```

Otkriti koja se tipkovnica koristi pokretanjem naredbe `dmesg | grep uhid`.

Naredbom `usbhidctl -f /dev/uhid0 -l -v -a` otkriti multimedijske tipke, npr.:
```
Generic_Desktop:System_Control.Generic_Desktop:System_Sleep=0
Consumer:Consumer_Control.Consumer:Mute=0
Consumer:Consumer_Control.Consumer:Scan_Next_Track=0
Consumer:Consumer_Control.Consumer:Scan_Previous_Track=0
Consumer:Consumer_Control.Consumer:Play/Pause=0
Consumer:Consumer_Control.Consumer:Volume_Increment=0
Consumer:Consumer_Control.Consumer:Volume_Decrement=0
```

Otkrivene tipke upisati u datoteku `/etc/usbhidaction.conf`:
```
# item (event)                  value   debounce        action
Generic_Desktop:System_Sleep    1       0               xdotool key XF86PowerOff
Consumer:Mute                   1       0               xdotool key XF86AudioMute
Consumer:Scan_Next_Track        1       0               xdotool key XF86AudioNext
Consumer:Scan_Previous_Track    1       0               xdotool key XF86AudioPrev
Consumer:Play/Pause             1       0               xdotool key XF86AudioPlay
Consumer:Volume_Increment       1       0               xdotool key XF86AudioRaiseVolume
Consumer:Volume_Decrement       1       0               xdotool key XF86AudioLowerVolume
```

Podesiti da se naredba `/usr/bin/usbhidaction -f /dev/uhid0 -c /etc/usbhidaction.conf -d` izvrši prilikom pokretanja desktopa.

## Održavanje sustava

Skinuti izvorni kod ako već nije instaliran:
```bash
fetch ftp://putanja_do_ src.txz
tar -xzvf src.txz -C /
```

Ažuriranje OS-a:
```bash
freebsd-update fetch
freebsd-update install
```

Ažuriranje programa:
```bash
pkg update
pkg upgrade
pkg clean
pkg clean -a
pkg autoremove
```

## Postavke firewall-a

Dodati sljedeću liniju u `/etc/rc.conf`:
```
pf_enable="YES"
```

Dodati sljedeće linije u `/etc/pf.conf`:
```
icmp_services = "{ unreach, redir, timex, echoreq }"
tcp_services = "{ 1716:1764 }"
udp_services = "{ mdns, 1716:1764 }"
scrub in all
block in all
pass out all keep state
pass in inet proto icmp all icmp-type $icmp_services keep state
#pass in proto tcp to any port $tcp_services keep state
pass in proto udp to any port $udp_services keep state
set skip on lo0
```

Popis ostalih portova: 

 - KDE Connect - TCP i UDP port 1716

Opis najčešćih naredbi:

 - pfctl -vnf /etc/pf.conf - Provjeri pravila ali ih nemoj učitati
 - pfctl -F all -f /etc/pf.conf - Ponovno učitaj pravila
 - pfctl -e - uključi firewall
 - pfctl -d - isključi firewall
 - pfctl -sr - izlistaj pravila

## Virtualizacija

Instalirati potrebne pakete:
```bash
pkg install bhyve-firmware virt-manager grub2-bhyve
```

Dodati sljedeću liniju u `/etc/rc.conf`:
```
kld_list="vmm nmdm if_tap if_bridge"
libvirtd_enable="YES"
```

U datoteku `/boot/loader.conf` upisati sljedeće:
```
if_bridge_load="YES"
if_tap_load="YES"
nmdm_load="YES"
vmm_load="YES"
```

## Instalacija iz portova (izvornog koda)

Početno postavljanje i kasnije periodično ažuriranje:
```bash
portsnap auto
```

Provjera novih verzija programa:
```bash
pkg version -l "<"
```

Instalacija programa:
```bash
cd /usr/ports/graphics/drm-fbsd12.0-kmod
make
make install
make clean
```

## Podešavanje IntelliJ IDEA

Prvo je potrebno instalirati najnoviji JAVA JDK.

Iz datoteke `idea-IU-X.X.X/bin/idea64.vmoptions` maknuti red `-XX:+UseConcMarkSweepGC`.

Instalirati `pty4j` na sljedeći način:
```bash
git clone https://github.com/JetBrains/pty4j.git
cd pty4j/native
gcc -fPIC -c .c
gcc -shared -o libpty.so .o
cp libpty.so idea-IU-X.X.X/lib/pty4j-native/freebsd/x86-64/
```

Instalirati `fsnotifier`:
```bash
git clone https://github.com/JetBrains/intellij-community/tree/master/native/fsNotifier/linux
./make.sh
```

ili bolje

```bash
pkg install intellij-fsnotifier
cp /usr/local/intellij/bin/fsnotifier-freebsd idea-IU-X.X.X/bin/
```

Nakon toga u izborniku `Help` odabrati `Edit Custom Properties` i upisati sljedeće:
```
idea.filewatcher.executable.path = fsnotifier-freebsd
```

U slučaju da fontovi ne izgledaju dobro (IDE ili Editor), podesiti antialiasing na Greyscale.

## VirtualBox

Instalirati VirtualBox:
```bash
pkg install virtualbox-ose
```

Dodati sljedeći red u datoteku `/boot/loader.conf`:
```
vboxdrv_load="YES"
```

Dodati sljedeći red u datoteku `/etc/rc.conf`:
```
vboxnet_enable="YES"
```

Postavka koju je potrebno upisati u `/etc/devfs.rules`:
```
[system=10]
add path 'usb/*' mode 0660 group operator
```

Provjeriti da li se korisnik već nalazi u grupi `operator`. Dodati korisnika u grupu `vboxusers`.
```bash
pw group mod vboxusers -m USERNAME
```

## TODO
 
 - XFCE4
