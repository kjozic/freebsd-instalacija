# FreeBSD 13.1 instalacija

- [FreeBSD 13.1 instalacija](#freebsd-131-instalacija)
  - [Instalacija](#instalacija)
    - [Instalacija operacijskog sustava](#instalacija-operacijskog-sustava)
    - [Instalacija bootloader-a](#instalacija-bootloader-a)
  - [Postavke operacijskog sustava](#postavke-operacijskog-sustava)
    - [Offline instalacija paketa](#offline-instalacija-paketa)
    - [Postavljanje hrvatskog jezika prilikom prijave korisnika](#postavljanje-hrvatskog-jezika-prilikom-prijave-korisnika)
    - [Potrebni servisi](#potrebni-servisi)
    - [Dozvole za pristup uređajima](#dozvole-za-pristup-uređajima)
    - [Naziv računala](#naziv-računala)
    - [Firewall](#firewall)
    - [Optimizacije za upotrebu na desktop računalima](#optimizacije-za-upotrebu-na-desktop-računalima)
  - [Rješavanje problema](#rješavanje-problema)
    - [Korištenje AMD grafičke kartice ako sustav koristi UEFI](#korištenje-amd-grafičke-kartice-ako-sustav-koristi-uefi)
    - [Trzanje zvuka](#trzanje-zvuka)
    - [Zastajkivanja u radu sustava](#zastajkivanja-u-radu-sustava)
  - [Instalacija desktop okruženja](#instalacija-desktop-okruženja)
    - [Instalacija MATE desktopa](#instalacija-mate-desktopa)
    - [Instalacija GNOME3 desktopa](#instalacija-gnome3-desktopa)
    - [Instalacija KDE desktopa](#instalacija-kde-desktopa)
  - [Postavke desktop okruženja](#postavke-desktop-okruženja)
    - [Fish ljuska](#fish-ljuska)
    - [Postavke fontova](#postavke-fontova)
    - [MATE postavke](#mate-postavke)
    - [GNOME3 postavke](#gnome3-postavke)
    - [KDE postavke](#kde-postavke)
    - [Multimedijska tipkovnica](#multimedijska-tipkovnica)
  - [Instalacija iz portova (izvornog koda)](#instalacija-iz-portova-izvornog-koda)
  - [Održavanje sustava](#održavanje-sustava)
  - [Instalacija uređaja](#instalacija-uređaja)
    - [Testiranje hardware-a](#testiranje-hardware-a)
    - [Printer](#printer)
    - [Web kamera](#web-kamera)
    - [Mrežna kartica RTL8125](#mrežna-kartica-rtl8125)
    - [Grafička kartica Intel UHD 750](#grafička-kartica-intel-uhd-750)
    - [Korištenje drivera za UEFI framebuffer](#korištenje-drivera-za-uefi-framebuffer)
  - [Virtualizacija](#virtualizacija)
    - [Bhyve](#bhyve)
    - [VirtualBox](#virtualbox)
  - [Podešavanje raznih programa](#podešavanje-raznih-programa)
    - [IntelliJ IDEA](#intellij-idea)

## Instalacija

### Instalacija operacijskog sustava

Skini ISO ili bilo koji drugi tip slike i spremi ga na USB disk ili CD/DVD.

Prilikom instalacije paziti na sljedeće detalje:

 - Zauzeće diska: cijeli disk
 - Particijska tablica: GPT
 - Swap particija: Ne
 - Datotečni sustav: UFS2, uključiti TRIM

Podesiti repozitorij paketa na najnoviji (zamijeniti "quarterly" sa "latest") u datoteci `/etc/pkg/FreeBSD.conf`.

Pokrenuti naredbu `pkg` da se instalira upravitelj paketima. Nakon toga pokrenuti `pkg update` da se ažurira popis raspoloživih programa.

Instalirati nužne pakete:
```bash
pkg install nano xorg fish doas webfonts drm-kmod sddm cups xsane sane-backends \
            avahi avahi-libdns nss_mdns
```

Dodati korisnika u sljedeće grupe:
```bash
pw group mod video -m USERNAME
pw group mod wheel -m USERNAME
pw group mod operator -m USERNAME
```

Promijeniti ljusku korisnika na fish:
```bash
chsh -s fish USERNAME
```

Ovlastiti korisnika kao superuser-a na način da se uredi datoteka `/usr/local/etc/doas.conf` i doda sljedeći red:
```
permit USERNAME as root
```

Učitati upravljački program za AMD-ovu grafičku karticu na način da se u datoteku `/etc/rc.conf` doda sljedeći red:
```
kld_list="amdgpu"       #AMD
kld_list="i915kms"      #Intel

```

Dodati sljedeću liniju u `/etc/fstab`:
```
proc    /proc   procfs  rw  0   0
```

Dodati sljedeću liniju u `/etc/rc.conf`:
```
avahi_daemon_enable="YES"
cupsd_enable="YES"
```

Instalirati ostale programe:
```bash
pkg install firefox chromium vlc go kicad cockroach node yarn git \
            libreoffice blender redshift python3 hplip gimp vscode \
            p7zip screenfetch gitg unrar kicad-library-footprints \
            kicad-library-packages3d kicad-library-symbols \
            kicad-library-tmpl qbittorrent lyx texlive-full xmedcon
            imagemagick7 kdeconnect-kde k3b kio-gdrive npm
```

### Instalacija bootloader-a

Ako se koristi neki drugi sustav, npr. Linux, potrebno je omogućiti da se FreeBSD može odabrati u izborniku boot manager-a.

U datoteci `/etc/grub.d/40-custom` upisati sljedeće:
```
exec tail -n +3 $0

menuentry "FreeBSD" {
insmod ufs2
set root=(hd3,msdos1)
kfreebsd=/boot/loader
}

```

Pokrenuti naredbu `update-grub2`.

## Postavke operacijskog sustava

### Offline instalacija paketa

Program `pkg` na početku nije instaliran već samo istoimena skripta. Program se instalira prilikom prvog pokretanja. Ukoliko računalo nije na Internetu, a paketi se moraju instalirati potrebno je skinuti pkg i sve ostale pakete koje je potrebno instalirati:  

```bash
fetch http://pkg.freebsd.org/FreeBSD:13:amd64/latest/All/pkg-1.18.3.pkg
SIGNATURE_TYPE="none" pkg add ./pkg-1.18.3.pkg
```

Nazivi i verzije programa mogu se pronaći na stranici [FreshPorts](https://www.freshports.org/).

Ako se paketi prenose preko USD diska, on se montira na slijedeći način:
```bash
mount -t msdosfs /dev/da0s1 /MOUNTPOINT
```

### Postavljanje hrvatskog jezika prilikom prijave korisnika

U datoteku `/etc/login.conf` na kraj zapisa default dodati sljedeće redove:
```
:charset=UTF-8:\
:lang=hr_HR.UTF-8:
```

Prvi red je obvezan, no drugi treba postaviti samo ako se žele hrvatske postavke sustava.

**Napomena:** Ne zaboraviti staviti znak \ na kraj zadnjeg reda prije dodavanja navedenih redova.

Pokrenuti naredbu `cap_mkdb /etc/login.conf`.

### Potrebni servisi

Bitne postavke koje je potrebno upisati u `/etc/rc.conf` (ako to već nije napravljeno):
```
clear_tmp_enable="YES"
hostname="GlavniPC"
keymap="hr.kbd"
ifconfig_igb0="DHCP"
ntpd_enable="YES"
powerd_enable="YES"
zfs_enable="YES"
devfs_system_ruleset="system"
```

Ako je potrebno promijeniti veličinu paketa, potrebno je dodati MTU parametar u postavkama mreže, npr. `ifconfig_igb0="DHCP mtu 1492"`.

### Dozvole za pristup uređajima

Postavke koje je potrebno upisati u `/etc/devfs.rules`:
```
[system=10]
add path 'cd*' mode 0666
add path 'pass*' mode 0666
add path 'xpt0' mode 0666
add path 'video*' mode 0666
add path 'uhid*' mode 0666
```

### Naziv računala

Postavke koje je potrebno upisati u `/etc/hosts`:
```
::1                     localhost localhost.my.domain GlavniPC
127.0.0.1               localhost localhost.my.domain GlavniPC
```

### Firewall

Dodati sljedeću liniju u `/etc/rc.conf`:
```
pf_enable="YES"
```

Dodati sljedeće linije u `/etc/pf.conf`:
```
icmp_services = "{ unreach, redir, timex, echoreq }"
tcp_services = "{ 1716:1764 }"
udp_services = "{ mdns, 1716:1764 }"
scrub in all
block in all
pass out all keep state
pass in inet proto icmp all icmp-type $icmp_services keep state
#pass in proto tcp to any port $tcp_services keep state
pass in proto udp to any port $udp_services keep state
set skip on lo0
```

Popis ostalih portova: 

 - KDE Connect - TCP i UDP port 1716

Opis najčešćih naredbi:

 - pfctl -vnf /etc/pf.conf - Provjeri pravila ali ih nemoj učitati
 - pfctl -F all -f /etc/pf.conf - Ponovno učitaj pravila
 - pfctl -e - uključi firewall
 - pfctl -d - isključi firewall
 - pfctl -sr - izlistaj pravila

### Optimizacije za upotrebu na desktop računalima

U datoteku `/boot/loader.conf` upisati sljedeće:
```
kern.maxproc="100000"
kern.ipc.shmseg="1024"
kern.ipc.shmmni="1024"
net.link.ifqmaxlen="2048"
net.inet.tcp.soreceive_stream="1"
```

## Rješavanje problema

### Korištenje AMD grafičke kartice ako sustav koristi UEFI

Dodati sljedeći red u datoteku `/boot/loader.conf`:
```
hw.syscons.disable=1
```

### Trzanje zvuka

Ako zvučna kartica radi probleme, dodati sljedeću liniju u `/etc/sysctl.conf`:
```
dev.hdac.%d.polling=1
```
gdje je `%d` redni broj zvučne kartice koja uzrokuje probleme (redne brojeve saznati sa `sysctl dev.hdac`).

### Zastajkivanja u radu sustava

Ako rad u grafičkom okruženju šteka, potrebno je promijeniti timer sa `LAPIC` na `HPET` (ili obratno) dodavanjem sljedećeg zapisa u datoteku `/etc/sysctl.conf`:
```
kern.eventtimer.timer=HPET
```

Odabrati onog koji ima najveću kvalitetu (najviši broj): `sysctl kern.eventtimer | grep quality`.


Ako zbog promjene timera sustav počne sporije reagirati, treba vratiti timer na prethodni i isključiti tickeless način rada na način da se uključi periodični timera u datoteci `/etc/sysctl.conf`:
```
kern.eventtimer.periodic=1
```

## Instalacija desktop okruženja

### Instalacija MATE desktopa

Instalirati MATE sa `pkg install mate`.

Dodati sljedeće linije u `/etc/rc.conf`:
```
hald_enable="YES"
dbus_enable="YES"
sddm_enable="YES"
```

### Instalacija GNOME3 desktopa

Instalirati GNOME3 sa `pkg install gnome3`.

Dodati sljedeće linije u `/etc/rc.conf`:
```
hald_enable="YES"
dbus_enable="YES"
gdm_enable="YES"
gnome_enable="YES"
sddm_enable="YES"
```

### Instalacija KDE desktopa

Instalirati KDE sa `pkg install kde5`.

U slučaju da se želi minimalna instalacija, potrebno je sa instalirati sljedeće pakete:
```bash
pkg install kde-baseapps plasma5-plasma kdeutils kdegraphics kmix plasma5-sddm-kcm
```

Dodati sljedeće linije u `/etc/rc.conf`:
```
hald_enable="YES"
dbus_enable="YES"
sddm_enable="YES"
```

## Postavke desktop okruženja

### Fish ljuska
Pokrenuti podešavanje fish ljuske naredbom `fish_config` i odabrati lava temu.

Ako se ne koristi disply manager (sddm ili sličan) potrebno je u datoteku `.xinitrc` upisati sljedeće:
```
exec mate-session
ili
exec xfce4-session
ili
exec cinnamon-session
itd.
```

### Postavke fontova

Da bi se poboljšao izgled fontova potrebno je isključiti bitmapirane fontove na sljedeći način:
```bash
cd /usr/local/etc/fonts/conf.d
ln -s ../conf.avail/70-no-bitmaps.conf
fc-cache -f
```

### MATE postavke

U datoteci `/usr/local/etc/PolicyKit/PolicyKit.conf` dodati kao prvi unos unutar korjenskog zapisa:
```xml
<match action="org.freedesktop.hal.storage.mount-removable">
<return result="yes"/>
</match>
<match action="org.freedesktop.hal.storage.mount-fixed">
<return result="yes"/>
</match>
```

Pokrenuti sljedeću naredbu:
```bash
gconftool-2 --set "/system/storage/default_options/vfat/mount_options" \
            --type list --list-type=string "[longnames,-u=,-L=en_US.UTF-8]"
```

Ostale postavke:
 - Podesiti subpixel hinting na full
 - Postaviti hrvatsku tipkovnicu
 - Postaviti fontove na 12
 - Postaviti da se pokreće Redshift prilikom pokretanja desktopa (`redshift -l geoclue2 -t 6500:4500`)

### GNOME3 postavke

Općenite postavke:

 - Hrvatska tipkovnica
 - Nightshift

### KDE postavke

Općenite postavke:

 - Hrvatska tipkovnica
 - Nightshift

### Multimedijska tipkovnica

Instalirati potrebne pakete:
```bash
pkg install xdotool
```

Otkriti koja se tipkovnica koristi pokretanjem naredbe `dmesg | grep uhid`.

Naredbom `usbhidctl -f /dev/uhid0 -l -v -a` otkriti multimedijske tipke, npr.:
```
Generic_Desktop:System_Control.Generic_Desktop:System_Sleep=0
Consumer:Consumer_Control.Consumer:Mute=0
Consumer:Consumer_Control.Consumer:Scan_Next_Track=0
Consumer:Consumer_Control.Consumer:Scan_Previous_Track=0
Consumer:Consumer_Control.Consumer:Play/Pause=0
Consumer:Consumer_Control.Consumer:Volume_Increment=0
Consumer:Consumer_Control.Consumer:Volume_Decrement=0
```

Otkrivene tipke upisati u datoteku `/etc/usbhidaction.conf`:
```
# item (event)                  value   debounce        action
Generic_Desktop:System_Sleep    1       0               xdotool key XF86PowerOff
Consumer:Mute                   1       0               xdotool key XF86AudioMute
Consumer:Scan_Next_Track        1       0               xdotool key XF86AudioNext
Consumer:Scan_Previous_Track    1       0               xdotool key XF86AudioPrev
Consumer:Play/Pause             1       0               xdotool key XF86AudioPlay
Consumer:Volume_Increment       1       0               xdotool key XF86AudioRaiseVolume
Consumer:Volume_Decrement       1       0               xdotool key XF86AudioLowerVolume
```

Podesiti da se naredba `/usr/bin/usbhidaction -f /dev/uhid0 -c /etc/usbhidaction.conf -d` izvrši prilikom pokretanja desktopa.

## Instalacija iz portova (izvornog koda)

Početno postavljanje i kasnije periodično ažuriranje:
```bash
portsnap auto
```

Provjera novih verzija programa:
```bash
pkg version -l "<"
```

Instalacija programa:
```bash
cd /usr/ports/graphics/drm-fbsd13-kmod
make
make install
make clean
```

## Održavanje sustava

Sa popisa [mirrora](https://docs.freebsd.org/en/books/handbook/mirrors/
) odabrati jedan od servera i sa njega skinuti izvorni kod (ako već nije instaliran):
```bash
fetch http://ftp11.freebsd.org/pub/FreeBSD/snapshots/amd64/13.1-STABLE/src.txz
tar -xzvf src.txz -C /
```

Ažuriranje OS-a:
```bash
freebsd-update fetch
freebsd-update install
```

Ažuriranje programa:
```bash
pkg update
pkg upgrade
pkg clean
pkg clean -a
pkg autoremove
```

## Instalacija uređaja

### Testiranje hardware-a

Pregled prepoznatog hardware i slanje rezultata na Internet radi se na slijedeći način:
```bash
hw-probe --all --upload
```

### Printer

Otvoriti stranicu `http://localhost:631` i dodati printer HP Smart Tank 510 sa PPD datotekom `/usr/local/share/ppd/HP/hp-smart_tank_510_series.ppd.gz`.
Instalirati printer kroz HPLIP naredbom `hp-setup -i 192.168.5.38`.

### Web kamera

Pomoću naredbe `usbconfig` saznati ime uređaja. Ime uređaja se kasnije upisuje u datoteku `/etc/rc.conf`.

Dodati sljedeće linije u `/etc/rc.conf`:
```
webcamd_enable="YES"
webcamd_device_0_name="Vimicro Corp. Venus USB2.0 Camera"
```

ili

```
webcamd_enable="YES"
webcamd_0_flags="-d ugen0.2"
```

Dodati sljedeće linije u `/boot/loader.conf`:
```
cuse_load="YES"
```

Izvršiti naredbu `pw groupmod webcamd -m USERNAME`

### Mrežna kartica RTL8125

Kako računalo ne može otići na Internet bez mrežne kartice, potrebno je skinuti pakete kako je opisano u poglavlju [Offline instalacija paketa](#offline-instalacija-paketa).

```bash
fetch http://pkg.freebsd.org/FreeBSD:13:amd64/latest/All/realtek-re-kmod-196.04.pkg
pkg install ./realtek-re-kmod-196.04.pkg

```

U datoteku `/boot/loader.conf` upisati sljedeće:
```
if_re_load="YES"
if_re_name="/boot/modules/if_re.ko"
```

Dodati sljedeću liniju u `/etc/rc.conf`:
```
ifconfig_re0="DHCP"
```

### Grafička kartica Intel UHD 750

Kako paketi sadrže staru verziju upravljačkih programa, instalacija se radi iz portova. Potrebno je instalirati izvorni kod sustava i portova kako je pisano u poglavljima [Održavanja sustava](#održavanje-sustava) i [Instalacija iz portova (izvornog koda)](#instalacija-iz-portova-izvornog-koda).

```bash
portsnap auto
cd /usr/ports/graphics/drm-510-kmod
make
make install
make clean
pkg install gpu-firmware-intel-kmod-rocketlake
```

**Napomena:** Ne dirati Xorg konfiguracijske datoteke.

### Korištenje drivera za UEFI framebuffer

U slučaju da grafička kartica nije podržana, može se koristiti upravljački program za generički UEFI framebuffer.  
Da bi se saznale podržane rezolucije potrebno je ući u konzolu prije podizanja sustava i isprobati slijedeće naredbe:
```bash
gop list    # Izlistava sve grafičke modove
gop set X   # Postavlja željeni grafički mode
gop get     # Ispisuje trenutni grafički mode
mode        # Izlistava sve tekstualne modove
mode X      # Postavlja tekstualni mode
```

U konzoli su prihvaćena oba načina postavljanja moda. Međutim, `/boot/loader.conf` prihvaća samo postavljanje tekstualnog moda:
```
exec="mode 3"
```

Potrebno je instalirati slijedeći paket:
```bash
pkg install xf86-video-scfb
```

Potrebno je napraviti datoteku `/usr/local/etc/X11/xorg.conf.d/driver-scfb.conf` sa slijedećim sadržajem:
```
Section "Device"
	Identifier "Card0"
	Driver	"scfb"
EndSection
```

## Virtualizacija

### Bhyve

Instalirati potrebne pakete:
```bash
pkg install bhyve-firmware virt-manager grub2-bhyve
```

Dodati sljedeće liniju u `/etc/rc.conf`:
```
kld_list="vmm nmdm if_tap if_bridge"
libvirtd_enable="YES"
```

U datoteku `/boot/loader.conf` upisati sljedeće:
```
if_bridge_load="YES"
if_tap_load="YES"
nmdm_load="YES"
vmm_load="YES"
```

### VirtualBox

Instalirati VirtualBox:
```bash
pkg install virtualbox-ose
```

Dodati sljedeći red u datoteku `/boot/loader.conf`:
```
vboxdrv_load="YES"
```

Dodati sljedeći red u datoteku `/etc/rc.conf`:
```
vboxnet_enable="YES"
```

Postavka koju je potrebno upisati u `/etc/devfs.rules`:
```
[system=10]
add path 'usb/*' mode 0660 group operator
```

Provjeriti da li se korisnik već nalazi u grupi `operator`. Dodati korisnika u grupu `vboxusers`.
```bash
pw group mod vboxusers -m USERNAME
```

## Podešavanje raznih programa

### IntelliJ IDEA

Prvo je potrebno instalirati najnoviji JAVA JDK.

Iz datoteke `idea-IU-X.X.X/bin/idea64.vmoptions` maknuti red `-XX:+UseConcMarkSweepGC`.

Instalirati `pty4j` na sljedeći način:
```bash
git clone https://github.com/JetBrains/pty4j.git
cd pty4j/native
gcc -fPIC -c .c
gcc -shared -o libpty.so .o
cp libpty.so idea-IU-X.X.X/lib/pty4j-native/freebsd/x86-64/
```

Instalirati `fsnotifier`:
```bash
git clone https://github.com/JetBrains/intellij-community/tree/master/native/fsNotifier/linux
./make.sh
```

ili bolje

```bash
pkg install intellij-fsnotifier
cp /usr/local/intellij/bin/fsnotifier-freebsd idea-IU-X.X.X/bin/
```

Nakon toga u izborniku `Help` odabrati `Edit Custom Properties` i upisati sljedeće:
```
idea.filewatcher.executable.path = fsnotifier-freebsd
```

U slučaju da fontovi ne izgledaju dobro (IDE ili Editor), podesiti antialiasing na Greyscale.
